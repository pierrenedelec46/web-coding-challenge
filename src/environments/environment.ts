/* istanbul ignore file */
export const environment = {
  API_URL : "https://jsonplaceholder.typicode.com/",
  PHOTOS_ENDPOINT : "photos",
  USERS_ENDPOINT: "users"
};
