import { ValidatorFn, AbstractControl, ValidationErrors, FormGroup, FormControl } from "@angular/forms";

/**
 * FormGroup validator
 * @param toValidateField
 * @param fields
 * @returns
 */
export const notContainsValidator = (toValidateField: string, fields: string[]): ValidatorFn => {
  return (group: AbstractControl): ValidationErrors | null => {
    if (!(group instanceof FormGroup)) {
      console.warn('[notContainsValidator] should be use as FormGroup Validator')
      return null;
    }
    const toValidateControl = group.get(
      toValidateField
    ) as FormControl<string>;

    if (!toValidateControl || toValidateControl.invalid){
      return null;
    }

    const substrings: { value: string; field: string }[] = [];

    fields.forEach((field) => {
      if (group.contains(field)) {
        const value = group.get(field)?.value;
        if (value) {
          substrings.push({ value, field });
        }
      }
    });

    if (substrings.length === 0) {
      return null;
    }

    const foundFields = substrings
      .filter(
        ({ value, field }) =>
          toValidateControl.value
            .toUpperCase()
            .includes(value.toUpperCase()) ||
          toValidateControl.value
            .toLowerCase()
            .includes(value.toLowerCase()) ||
          toValidateControl.value.includes(value)
      )
      .map(({ field }) => field);

    return foundFields.length > 0
      ? {
          notContains: `${toValidateField} must not contains ${foundFields.join(
            ' and/or '
          )}`,
        }
      : null;
  };
}
