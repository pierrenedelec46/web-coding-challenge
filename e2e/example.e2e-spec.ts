import { test, expect } from '@playwright/test';


test.beforeEach(async({page})=>  await page.goto('/'))

test('fill form correctly and button should be enabled', async ({ page }) => {
  await page.locator("input[name='firstName']").fill('Pierre')
  await page.locator("input[name='lastName']").fill('Nédélec')
  await page.locator("input[name='email']").fill('pierre@doe.com')
  await page.locator("input[name='new-password']").fill('abcdefgH')
  await page.locator("input[name='new-password']").blur()
  const button = await page.locator("button[role='button']")
  expect(button).toBeTruthy()
  expect(await button.isEnabled()).toBeTruthy()
});

test('fill form incorrectly and button should be disabled', async ({ page }) => {
  await page.locator("input[name='firstName']").fill('Pierre')
  await page.locator("input[name='lastName']").fill('Nédélec')
  await page.locator("input[name='email']").fill('pierre@doe.com')
  await page.locator("input[name='new-password']").fill('abcdefg')
  await page.locator("input[name='new-password']").blur()
  const button = await page.locator("button[type='submit']")
  expect(button).toBeTruthy()
  expect(await button.isDisabled()).toBeTruthy()
});

test('click on password icon show reveal/hide password and update aria-label', async ({ page }) => {
  const password = await page.locator("input[name='new-password']")
  await password.fill('abcdefg')
  const iconBefore = await page.locator("i[role='button']")
  await expect(iconBefore).toBeTruthy()
  await expect(password).toHaveAttribute('type', 'password')
  let ariaLabelIcon = await iconBefore.getAttribute('aria-label')
  await expect(ariaLabelIcon).toContain('Show password')
  await iconBefore.click()
  const iconAfter = await page.locator("i[role='button']")
  ariaLabelIcon = await iconAfter.getAttribute('aria-label')
  console.log(ariaLabelIcon)
  await expect(ariaLabelIcon).toContain('Hide password')
  await expect(password).toHaveAttribute('type', 'text')
  await iconAfter.click()
  const iconfinal = await page.locator("i[role='button']")
  await expect(password).toHaveAttribute('type', 'password')
  await expect(await iconfinal.getAttribute('aria-label')).toContain('Show password')
});

test('firstName error', async ({ page }) => {
  const firstName = await page.locator("input[name='firstName']")
  await firstName.fill('')
  await firstName.blur()
  const labelfirstName = await page.locator("label[for='firstName']")
  await expect(labelfirstName).toHaveClass('error')
  expect(await page.$eval("input[name='firstName']", e => getComputedStyle(e).borderColor)).toBe('rgb(123, 12, 23)');
  expect(await page.$eval("label[for='firstName']", e => getComputedStyle(e).color)).toBe('rgb(123, 12, 23)');
  await expect(page.locator("input > div.error")).toBeDefined()
});

test('lastName error', async ({ page }) => {
  const lastName = await page.locator("input[name='lastName']")
  await lastName.fill('')
  await lastName.blur()
  const labellastName = await page.locator("label[for='lastName']")
  await expect(labellastName).toHaveClass('error')
  expect(await page.$eval("input[name='lastName']", e => getComputedStyle(e).borderColor)).toBe('rgb(123, 12, 23)');
  expect(await page.$eval("label[for='lastName']", e => getComputedStyle(e).color)).toBe('rgb(123, 12, 23)');
  await expect(page.locator("input > div.error")).toBeDefined()
});

test('email error', async ({ page }) => {
  const email = await page.locator("input[name='email']")
  await email.fill('')
  await email.blur()
  const labelemail = await page.locator("label[for='email']")
  await expect(labelemail).toHaveClass('error')
  expect(await page.$eval("input[name='email']", e => getComputedStyle(e).borderColor)).toBe('rgb(123, 12, 23)');
  expect(await page.$eval("label[for='email']", e => getComputedStyle(e).color)).toBe('rgb(123, 12, 23)');
  await expect(page.locator("input > div.error")).toBeDefined()
});

test('password error', async ({ page }) => {
  const password = await page.locator("input[name='new-password']")
  await password.fill('')
  await password.blur()
  const labelpassword = await page.locator("label[for='new-password']")
  await expect(labelpassword).toHaveClass('error')
  expect(await page.$eval("input[name='new-password']", e => getComputedStyle(e).borderColor)).toBe('rgb(123, 12, 23)');
  expect(await page.$eval("label[for='new-password']", e => getComputedStyle(e).color)).toBe('rgb(123, 12, 23)');
  await expect(page.locator("input > div.error")).toBeDefined()
});

test('display fullName', async ({ page }) => {
  const firstName = await page.locator("input[name='firstName']")
  const lastName = await page.locator("input[name='lastName']")
  await firstName.fill('Pierre')
  await lastName.fill('Nédélec')
  await lastName.blur()
  const h2 = await page.locator(".form-header > h2")
  expect(await h2.innerText()).toContain('Pierre')
  expect(await h2.innerText()).toContain('Nédélec')
});

