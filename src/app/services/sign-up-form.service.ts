import { Injectable, inject } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  NonNullableFormBuilder,
  Validators,
} from '@angular/forms';
import {
  SignUpFormGroup,
  SignUpFormGroupValue,
} from '../interfaces/sign-up-form.interface';
import { combineLatest, debounceTime, distinctUntilChanged, map } from 'rxjs';
import { ApiService } from './api.service';
import { passwordStrengthValidator } from '../validators/password-strength.validator';
import { notContainsValidator } from '../validators/not-contains.validator';

@Injectable({
  providedIn: 'root',
})
export class SignUpFormService {
  private fb: NonNullableFormBuilder = inject(FormBuilder).nonNullable;
  private apiService = inject(ApiService);



  getForm(): FormGroup<SignUpFormGroup> {
    return this.fb.group<SignUpFormGroup>(
      {
        firstName: this.firstNameControl,
        lastName: this.lastNameControl,
        email: this.emailControl,
        password: this.passwordControl,
      },
      {
        validators: [
          notContainsValidator('password', ['firstName', 'lastName']),
        ],
      }
    );
  }

  get firstNameControl() {
    return this.fb.control('', Validators.required);
  }

  get lastNameControl() {
    return this.fb.control('', Validators.required);
  }

  get emailControl() {
    return this.fb.control('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]+$')]);
  }

  get passwordControl() {
    return this.fb.control('', [
      Validators.required,
      Validators.minLength(8),
      passwordStrengthValidator,
    ]);
  }




  fullName(formGroup: FormGroup<SignUpFormGroup>) {
   return combineLatest([
    formGroup.controls.firstName.valueChanges,
    formGroup.controls.lastName.valueChanges
  ])
   .pipe(
      distinctUntilChanged(),
      debounceTime(300),
      map(([firstName, lastName]) => firstName && lastName ? `${firstName} ${lastName}` : '')
    )
  }

  register(formGroupValue: SignUpFormGroupValue) {
    return this.apiService.register(formGroupValue)
  }
}
