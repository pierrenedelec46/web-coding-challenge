import { createHttpFactory, HttpMethod, SpectatorHttp } from '@ngneat/spectator/jest';
import { ApiService } from './api.service';
import { SignUpBody, SignUpFormGroupValue, SignUpPostResponse } from '../interfaces/sign-up-form.interface';
import { of } from 'rxjs';


describe('ApiService', () => {
  let spectator: SpectatorHttp<ApiService>;
  const createService = createHttpFactory(ApiService);

  beforeEach(() => spectator = createService());

  it('should have http service', () => {
    expect(spectator.service['http']).toBeDefined();
  });
  it('should have method getPhotoById', () => {
    expect(spectator.service.getPhotoById).toBeDefined();
  });
  it('getPhotoById(1) should perform get on jsonplaceholder.typicode.com/photos/1 api ', () => {
    spectator.service.getPhotoById(1).subscribe()
    spectator.expectOne('https://jsonplaceholder.typicode.com/photos/1', HttpMethod.GET)
  });
  it('should have method postUser', () => {
    expect(spectator.service.postUser).toBeDefined();
  });

  it('postUser should perform post on jsonplaceholder.typicode.com/users api ', () => {
    const body: SignUpBody = {
      firstName: 'Pierre',
      lastName: 'Nédélec',
      password: "123456",
      email: 'pierre@doe.com',
      thumbnailUrl : 'https://via.placeholder.com/150/92c952'
    }
    spectator.service.postUser(body).subscribe()
    const req = spectator.expectOne('https://jsonplaceholder.typicode.com/users', HttpMethod.POST)
    expect(req.request.body).toEqual(body);
  });
  it('register method testing', (done) => {
    const formGroupValue: SignUpFormGroupValue = {
      firstName: 'Pierre',
      lastName: 'Nédélec',
      email: 'pierre@doe.com',
      password: '123456'
    }
    const photo = {
      albumId: 1,
      id: 1,
      title: '1',
      url: 'https://url',
      thumbnailUrl: 'https://url'
    }
    const response: SignUpPostResponse = {
      id:1,
      firstName: 'Pierre',
      lastName: 'Nédélec',
      email: 'pierre@doe.com',
      password: '123456',
      thumbnailUrl: 'https://url'
    }
    const spyObj1 = jest.spyOn(spectator.service, 'getPhotoById').mockImplementation(() => of(photo))
    const spyObj2 = jest.spyOn(spectator.service, 'postUser').mockImplementation(() => of(response))

    spectator.service.register(formGroupValue).subscribe(
      (data)=> {
        expect(data).toEqual({
          firstName: 'Pierre',
          lastName: 'Nédélec',
          email: 'pierre@doe.com',
          password: '123456',
          id:1 , thumbnailUrl: 'https://url'})
        expect(data.thumbnailUrl).toEqual(photo.thumbnailUrl)
        expect(spyObj1).toHaveBeenCalledWith(formGroupValue.lastName.length)
        expect(spyObj2).toHaveBeenCalledWith({...formGroupValue, thumbnailUrl: photo.thumbnailUrl })
        done();
      })

  });

});
