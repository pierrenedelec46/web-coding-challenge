import { AbstractControl, ValidationErrors } from "@angular/forms";

/**
 * passwordStrenght
 * @returns
 */
export const passwordStrengthValidator = (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    if (!value) {
      return null;
    }
    const hasUpperCase = /[A-Z]+/.test(value);
    const hasLowerCase = /[a-z]+/.test(value);
    const strengthValid = hasLowerCase && hasUpperCase
    if (!strengthValid){
      return { passwordStrength: true }
    }
    return null;
  };

