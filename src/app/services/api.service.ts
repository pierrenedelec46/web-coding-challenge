import { Injectable, inject } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { SignUpBody, SignUpFormGroupValue, SignUpPostResponse } from '../interfaces/sign-up-form.interface';
import { Photo } from '../interfaces/photo.interface';
import { map, switchMap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private http = inject(HttpClient)

  getPhotoById(id: number | string){
    return this.http.get<Photo>(`${environment.API_URL}${environment.PHOTOS_ENDPOINT}/${id}`)
  }

  postUser(body: SignUpBody){
    return this.http.post<SignUpPostResponse>(`${environment.API_URL}${environment.USERS_ENDPOINT}`, body)
  }

  register(formGroupValue: SignUpFormGroupValue) {
    return this.getPhotoById(formGroupValue.lastName.length).pipe(
      map((photo: Photo) => ({
        ...formGroupValue,
        thumbnailUrl: photo.thumbnailUrl,
      })),
      switchMap((body: SignUpBody) => this.postUser(body))
    );
  }

}
