import { FormControl, FormGroup } from '@angular/forms';
import { AppComponent } from './app.component';
import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { Observable } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';


describe('AppComponent', () => {
  let spectator: Spectator<AppComponent>;
  const createComponent = createComponentFactory({
    component: AppComponent,
    imports: [HttpClientTestingModule]
  });

  beforeEach(() => spectator = createComponent());

  it('should have inject signupFormService', () => {
    expect(spectator.component['signUpFormService']).toBeDefined()
  });

  it('should have a formGroup', () => {
    expect(spectator.component.formGroup).toBeInstanceOf(FormGroup)
  });

  it('should have fullName$ Observable', () => {
    expect(spectator.component.fullName$).toBeInstanceOf(Observable)
  });

  it('should have getters for formControl', () => {
    expect(spectator.component.lastName).toBeInstanceOf(FormControl)
    expect(spectator.component.firstName).toBeInstanceOf(FormControl)
    expect(spectator.component.email).toBeInstanceOf(FormControl)
    expect(spectator.component.password).toBeInstanceOf(FormControl)
  });
  it('should have register Method', () => {
    expect(spectator.component.register).toBeDefined()
  });

  it('register should call signUpFormService.register with formGroup value', () => {
    spectator.component.formGroup.get('lastName')?.setValue('Nédélec')
    const spyObj = jest.spyOn(spectator.component['signUpFormService'], 'register')
    spectator.component.register()
    expect(spyObj).toHaveBeenCalledWith(spectator.component.formGroup.value)
  });

  it('should have showPassword Method ', () => {
    expect(spectator.component.showPassword).toBeDefined()
  });

  it('showPassword should update password input type to text', () => {
    const passwordInput = spectator.query("input[type='password']");
    let mockHTMLElement = document.createElement('i');
    spectator.component.showPassword((passwordInput as HTMLInputElement), mockHTMLElement);
    expect((passwordInput as HTMLInputElement).type).toEqual('text');
  });
  it('showPassword should update password input type to password if type is text', () => {
    const passwordInput = spectator.query("input[type='password']");
    (passwordInput as HTMLInputElement).type = 'text';
    let mockHTMLElement = document.createElement('i');
    spectator.component.showPassword((passwordInput as HTMLInputElement), mockHTMLElement);
    expect((passwordInput as HTMLInputElement).type).toEqual('password');
  });
});
