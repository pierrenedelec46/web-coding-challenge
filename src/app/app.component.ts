import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignUpFormService } from './services/sign-up-form.service';
import { ReactiveFormsModule } from '@angular/forms';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'wcc-root',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  private signUpFormService = inject(SignUpFormService);
  formGroup = this.signUpFormService.getForm();
  fullName$ = this.signUpFormService.fullName(this.formGroup);

  showPassword(passwordRef: HTMLInputElement, togglePasswordRef: HTMLElement) {
    if (passwordRef.type === 'password') {
      passwordRef.type = 'text';
      togglePasswordRef.setAttribute('aria-label', 'Hide password.');
    } else {
      passwordRef.type = 'password';
      togglePasswordRef.setAttribute(
        'aria-label',
        `Show password as plain text.
      Warning: this will display your password on the screen.
      `
      );
    }
  }

  async register() {
      await firstValueFrom(this.signUpFormService.register(this.formGroup.getRawValue()))
  }

  get lastName() {
    return this.formGroup.controls.lastName;
  }

  get firstName() {
    return this.formGroup.controls.firstName;
  }

  get email() {
    return this.formGroup.controls.email;
  }

  get password() {
    return this.formGroup.controls.password;
  }
}
