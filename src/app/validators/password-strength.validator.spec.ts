import { FormControl } from "@angular/forms";
import { passwordStrengthValidator } from "./password-strength.validator";

describe('passwordStrengthValidator', () => {
  let passwordCtrl: FormControl;
  beforeEach(() => {
    passwordCtrl  = new FormControl('PieRre11NédéLeC', passwordStrengthValidator)
  });

  it("passwordCtrl should be valid", () => {
    expect(passwordCtrl.valid).toBeTruthy()
    expect(passwordCtrl.errors).toBeNull()
  });
  it("passwordCtrl should be invalid for UpperCase missing", () => {
    passwordCtrl.setValue('pierre')
    expect(passwordCtrl.invalid).toBeTruthy()
    expect(passwordCtrl.hasError('passwordStrength')).toBeTruthy()
  });

  it("passwordCtrl should be invalid for LowerCase missing", () => {
    passwordCtrl.setValue('PIERRE')
    expect(passwordCtrl.invalid).toBeTruthy()
    expect(passwordCtrl.hasError('passwordStrength')).toBeTruthy()
  });

  it("passwordCtrl should be valid with empty value", () => {
    passwordCtrl.setValue('')
    expect(passwordCtrl.valid).toBeTruthy()
    expect(passwordCtrl.errors).toBeNull()
  });


})
