# WebCodingChallenge

Last Updated 08/11/2023 --> Angular 17.0.0 release date see branch v17 for next gen version

## Demo and Reports
- [Demo](https://web-coding-challenge-fe.web.app/)
- [Jest Unit Testing Coverage](https://coverage-web-coding-challenge-fe.web.app/)
- [Playwright E2E Testing Report](https://playwright-web-coding-challenge-fe.web.app/)
- [Webpack-Bundle-Analyzer Report](https://bundle-web-coding-challenge-fe.web.app/)
- [PA11Y Report](https://pa11y-web-coding-challenge-fe.web.app/)
- [Compodoc](https://compodoc-web-coding-challenge-fe.web.app/)

## Project Details
This project was generated with Angular CLI version 16.2.6 and Angular 16.2.9.

**System Requirements:**
- Node.js 18.13.0 or Node.js >= 20 ([More Info](https://github.com/angular/angular/blob/16.2.9/package.json))
- Pnpm 8.6.3+
- Supported OS: Windows 10+, MacOS 12 Monterey or MacOS 13 Ventura, Debian 11, Debian 12, Ubuntu 20.04, or Ubuntu 22.04.

## Development Server
- Run `pnpm start` for a development server.
- Access the application at `http://localhost:4200/`.
- The application will automatically reload when you change source files.

## Build
- Run `pnpm build` to build the project.
- Build artifacts will be stored in the `dist/` directory.

## Running Jest Unit Tests
- Run `pnpm test` to execute unit tests using Jest.
- Run `pnpm test:watch` for Jest unit tests in watch mode.
- Run `pnpm test:coverage` to execute Jest with code coverage.

## Running Playwright End-to-End Tests
- Run `pnpm e2e:test` to execute end-to-end tests via terminal.
- Run `pnpm e2e:report` to visualize the end-to-end report.
- Run `pnpm e2e:ui` to execute end-to-end tests via UI.

## Webpack-Bundle-Analyzer
- Run `pnpm serve:analyze` to build the app with production configuration and serve webpack-bundle-analyzer.
- Run `pnpm build:analyze` to build the app with production configuration and generate the webpack-bundle-analyzer report in the 'analyze' folder.

## Compodoc
- Run `pnpm serve:compodoc` to serve Compodoc.
- Run `pnpm build:compodoc` to build Compodoc as static files in the 'compodoc' folder.

## Pa11y
- Run `pnpm pa11y` to execute accessibility testing (WCAG2AAA) on the deploy URL.
- Run `pnpm pa11y:report` to execute accessibility testing and generate a report in the 'pa11y' folder.

## Project Generation
This project was generated with the following command:
```bash
ng new web-coding-challenge --routing false --package-manager pnpm --standalone true --style scss --prefix wcc
```

## Recent features of Angular 
* Standalone App 
* Standalone Component
* inject 
* Typed ReactiveForms 
* Angular 16 introduces an exciting change by providing experimental support for Jest see angular.json

## Possible Futures Improvements
 * Directive to manage error class 
 * Prettier/Eslint 
 * Signals
 * Storybook 
 * reusable library with secondary endpoints
 * reusable form field with component and ControlValueAccessor

## Features

  * The full name (first + last name) of the user should be shown in the UI outside of the form. It should use a
  single variable that is updated whenever the input values are changed : 

  *see fullName method in sign-up-form.service.ts and usage in app.component.html*

* All fields are required :

  *See Validators.required in sign-up-form.service.ts*

* Password validation:
    o Should be a minimum of eight characters
    o Should contain lower and uppercase letters
    o Should not contain the user's first or last name
  
  *See validators  ( not-contains.validator.ts && password-strength.validator.ts)  in validators folder and usage in sign-up-form.service.ts*

* Email should be validated but there are various ways of accomplishing this. So, show us what you consider as
a proper email validation :

  *See FAQ part below *

* HttpRequests : 
 
  *See api.service.ts*
  
# FAQ

## Why using Reactive forms ?

Reactive forms Provide direct, explicit access to the underlying form's object model. Compared to template-driven forms, they are more robust: they're more scalable, reusable, and testable. If forms are a key part of your application, or you're already using reactive patterns for building your application, use reactive forms.

## Interface vs Type ?

type aliases can act sort of like interfaces, however, there are 3 important differences ( union types, declaration merging)
use whatever suites you and your team, just be consistent
always use interface for public API's definition when authoring a library or 3rd party ambient type definitions


## What is new-password ? 

Why password constraints are displayed directly ?

see https://web.dev/articles/sign-in-form-best-practices


## Why not using Angular Built-in Validator Email ?

* Angular Built-in Validator accept abc@abc and this email is valid on HTML5 
  As you ask a proper `proper email validation` we should send an email or ely on external api for that like (https://www.abstractapi.com/api/email-verification-validation-api) , all of that required a Backend to secure Api Token or send an email. 
  As it is only a Frontend Challenge, i decide to keep RegExp Pattern Validator.

## How Web Accessibility was managed ? 


Red-green color blindness : 338 million people worldwide 

it's make sense to have label in error + errors text + input border-color + asterisk

Some Chrome extension were used like A11y.css / ARIA DevTools/ Axe DevTools / WAVE  and pa11y (https://github.com/pa11y/pa11y) with WCAG2AAA standard

Aria elements used : 
  * aria-hidden
  * aria-required
  * aria-describedby
  * aria-label
  * aria-disabled
  * role ( main/header/button)
  * tabindex="0"

