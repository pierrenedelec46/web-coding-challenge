import { createServiceFactory, SpectatorService } from '@ngneat/spectator/jest';
import { SignUpFormService } from './sign-up-form.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from './api.service';
import { notContainsValidator } from '../validators/not-contains.validator';
import { passwordStrengthValidator } from '../validators/password-strength.validator';

describe('SignUpFormService', () => {
  let spectator: SpectatorService<SignUpFormService>;
  const createService = createServiceFactory({
    service: SignUpFormService,
    mocks: [ApiService]
  });

  beforeEach(() => spectator = createService());


  it('should have getForm method and return a formGroup', () => {
    expect(spectator.service.getForm).toBeDefined();
    const form = spectator.service.getForm()
    expect(form).toBeInstanceOf(FormGroup)
    expect(form.controls.email).toBeDefined()
    expect(form.controls.lastName).toBeDefined()
    expect(form.controls.email).toBeDefined()
    expect(form.controls.password).toBeDefined()
    expect(form.hasValidator(notContainsValidator('password', ['firstName', 'lastName'])))
  });

  it('should have getters for retrieving individual formControl with validators', () => {
    const firstNameControl = spectator.service.firstNameControl
    expect(firstNameControl).toBeInstanceOf(FormControl);
    expect(firstNameControl.value).toEqual('');
    expect(firstNameControl.hasValidator(Validators.required)).toBeTruthy();

    const lastNameControl = spectator.service.lastNameControl
    expect(lastNameControl).toBeInstanceOf(FormControl);
    expect(lastNameControl.value).toEqual('');
    expect(lastNameControl.hasValidator(Validators.required)).toBeTruthy();

    const emailControl = spectator.service.emailControl
    expect(emailControl).toBeInstanceOf(FormControl);
    expect(emailControl.value).toEqual('');
    expect(emailControl.hasValidator(Validators.required)).toBeTruthy();
    emailControl.setValue('pierre@john.doe')
    expect(emailControl.valid).toBeTruthy()
    emailControl.setValue('pierre@john')
    expect(emailControl.invalid).toBeTruthy()


    const passwordControl = spectator.service.passwordControl
    expect(passwordControl).toBeInstanceOf(FormControl);
    expect(passwordControl.value).toEqual('');
    expect(passwordControl.hasValidator(Validators.required)).toBeTruthy();

    //https://github.com/angular/angular/issues/43454
    passwordControl.setValue('abcdefG')
    expect(passwordControl.invalid).toBeTruthy()

    expect(passwordControl.hasError('minlength')).toBeTruthy();
    passwordControl.setValue('abcdefGH')

    expect(passwordControl.hasValidator(passwordStrengthValidator)).toBeTruthy();

  });



  it('should have fullName method and return fullName', (done) => {
    expect(spectator.service.fullName).toBeDefined();
    const formGroup = spectator.service.getForm()
    const obs$ = spectator.service.fullName(formGroup)
    obs$.subscribe((fullName:string)=> {
      expect(fullName).toContain(formGroup.controls.firstName.value)
      expect(fullName).toContain(formGroup.controls.lastName.value)
      done();
    })

    formGroup.controls.firstName.setValue('Pierre')
    formGroup.controls.lastName.setValue('Nédélec')

  });

  it("fullName should return ''", (done) => {
    const formGroup = spectator.service.getForm()
    const obs$ = spectator.service.fullName(formGroup)
    obs$.subscribe((fullName:string)=> {
      expect(fullName).toEqual('')
      done();
    })

    formGroup.controls.firstName.setValue('')
    formGroup.controls.lastName.setValue('Nédélec')

  });

  it('register method should call api register', () => {
    const apiService = spectator.inject(ApiService);
    const formGroup = spectator.service.getForm()
    const spyObj1 = jest.spyOn(apiService, 'register')
    spectator.service.register(formGroup.getRawValue())
    expect(spyObj1).toHaveBeenCalledWith(formGroup.value)
  });

});
