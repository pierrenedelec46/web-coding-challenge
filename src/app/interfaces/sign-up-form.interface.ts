import { FormControl } from "@angular/forms"

export interface AbstractSignUpValue {
  firstName: string
  lastName: string
  email: string
}

export interface SignUpFormGroupValue extends AbstractSignUpValue{
  password: string
}

export interface SignUpFormGroup{
  firstName: FormControl<string>
  lastName: FormControl<string>
  email: FormControl<string>
  password: FormControl<string>
}


export interface SignUpBody extends SignUpFormGroupValue{
  thumbnailUrl: string
}

export interface SignUpPostResponse extends SignUpBody{
  id: number
}
